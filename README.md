# Proyecto2_premios



## Instrucciones

El documento adjunto contiene un enunciado el cual debe desarrollar en el lenguaje Java y utilizar PostgreSQL.
Debe crear un único proyecto que contenga todos los ejercicios, puede crear una clase para cada uno.
El proyecto debe estar estructurado en capas, objetos, además utilizar base de datos e interfaz gráfica.
Todo el proyecto debe estar en un repositorio de GitLab.
Entrega.

Todas las instrucciones de entrega están en el archivo PDF adjunto.
La fecha y hora límite de entrega es el domingo 23 de abril a las 05:00 PM.
Evaluación.

La entrega esta relacionada al porcentaje de proyecto correspondiente al curso.
El docente revisa cada proyecto con los equipos de trabajo bajo la modalidad de citas en días y horas específicas, y posterior a esa revisión se asigna la nota en base a la rúbrica.

- Apertura: martes, 21 de marzo de 2023, 18:00
- Cierre: domingo, 23 de abril de 2023, 17:00
